**Privacy Policy**   
Protecting your private information is our priority. This Statement of Privacy applies to www.registahealth.com and Regista App and governs data collection and usage. For the purposes of this Privacy Policy, unless otherwise noted, all references to Regista App include www.registahealth.com and Regista. The Regista App is a smart phone-based software that uses evidence-based artificial intelligence-driven algorithms to assist healthcare professionals predict, prevent and treat cancer in a timely manner. By using the Regista website and/or Regista App, you consent to the data practices described in this statement. 
  
**Collection of your Personal Information**  
In order to better assist healthcare providers in managing or preventing cancer, Regista collects personally identifiable information and information on certain symptoms and/or risk factors related to cancer. The information collected includes: 
  
•	First and Last Name  
•	Age  
•	Gender  
•	Race  
•	Ethnic group  
•	Residential and/or mailing address  
•	E-mail Address  
•	Phone Number  
•	Symptom(s)  
•	Risk factor(s) for cancer  

If you purchase a subscription to the Regista app, we collect billing and credit card information. This information is used to complete the purchase transaction. 

  
**How Patient Privacy is Stored and Protected**  

Regista handles all patient data according to the laws governing the Health Insurance Portability and Accountability Act we understand that information about a patient’s health is personal. Because of this, we strive to maintain the confidentiality of every patient’s health information. We strive to safeguard this information through administrative, physical and technical means, and otherwise abide by applicable federal and state guidelines. Our patient data is stored in a secured Amazon Web Service cloud that is only accessible to top management of Regista, Inc. This data is stored for the purpose of research and development, and for improving our machine learning algorithm in predicting and diagnosing cancer.

**How Website User Data is Protected**  
We do not collect any personal information about you on our website unless you voluntarily provide it to us. However, you may be required to provide certain personal information to us when you elect to use certain products or services available on the Site. These may include: (a) registering for an account on our Site; (b) entering a sweepstakes or contest sponsored by us or one of our partners; (c) signing up for special offers from selected third parties; (d) sending us an email message; (e) submitting your credit card or other payment information when ordering and purchasing products and services on our Site. To wit, we will use your information for, but not limited to, assisting health workers in making evidence-based and data-driven clinical decisions, communicating with you in relation to services and/or products you have requested from us. We also may gather additional personal or non-personal information in the future. 
  
**Sharing Information with Third Parties**  
Regista uses patient information in the context of what is allowable by law. 

**Cancer Prediction**  
Regista primarily uses the health information obtained to help health professionals predict a cancer diagnosis early and recommend measures to either refer and/or confirm the diagnosis. The record provided is stored in a secured AWS, and it is used primarily to improve Regista’s machine learning algorithm to improve the accuracy of its prediction.

Regista may disclose this information so that other doctors, nurses, and entities such as laboratories can meet the patient’s healthcare needs.

**Data Analysis and Machine Learning**  
Regista may share data with trusted partners to help perform statistical analysis and improve our cancer prediction algorithms, but any of such data shared will be deidentified, sharing no link to the patient. 

**Other Ways We Could Share Patient Information**  
We may also use your health information to:  
•	Comply with federal, state or local laws that require disclosure.  
•	Assist in public health activities such as tracking diseases or medical devices.  
•	Comply with federal and state health oversight activities such as fraud investigations.  
•	Respond to law enforcement officials or to judicial orders, subpoenas or other process.  
•	Inform coroners, medical examiners and funeral directors of information necessary for them to fulfill their duties.  

•	Conduct research following internal review protocols to ensure the balancing of privacy and research needs.  
•	Avert a serious threat to health or safety.  
•	Assist in specialized government functions such as national security, intelligence and protective services.  
•	Inform military and veteran authorities if you are an armed forces member (active or reserve).  
•	Inform a correctional institution if you are an inmate.  
•	Communicate within our organization for treatment, payment, or healthcare operations.  
•	Communicate with other providers, health plans, or their related entities for their treatment or payment activities, or health care operations activities relating to quality assessment and improvement, care coordination and the qualifications and training of healthcare professionals;  
•	Provide information to other third parties with whom we do business, such as a record storage provider. However, you should know that in these situations, we require third parties to provide us with assurances that they will safeguard your information.  
All other uses and disclosures, not previously described, may only be done with your written authorization. We will also obtain your authorization before we use or disclose your health information for marketing purposes or before we would sell your information. You may revoke your authorization at any time; however, this will not affect prior uses and disclosures. In some cases, state law may require that we apply extra protections to some of your health information.  
  

**What are the Healthcare Professional's Responsibilities?**  
We are required by law to:  
•	Maintain the privacy of your health information.  
•	Provide this Notice of our duties and privacy practices.  
•	Abide by the terms of the Notice currently in effect.  
•	Tell you if there has been a breach that compromises your health information.  
We reserve the right to change our privacy practices, and make the new practices effective for all the information we maintain. Revised notices will be posted on the Doctor On Demand website and mobile application.  

**Do you have any Federal Rights?**  
The law entitles you to:  
•	Inspect and copy certain portions of your health information. We may deny your request under limited circumstances. You may request that we provide your health records to you in an electronic format.  
•	Request amendment of your health information if you feel the health information is incorrect or incomplete. However, under certain circumstances we may deny your request.  
•	Receive an accounting of certain disclosures of your health information made for the prior six (6) years, although this excludes disclosures for treatment, payment, and health care operations. (Fees may apply to this request).  
•	Request that we restrict how we use or disclose your health information. However, we are not required to agree with your requests, unless you request that we restrict information provided to a payor, the disclosure would be for the payor's payment or healthcare operations, and you have paid for the health care services completely out of pocket.  
•	Request that we communicate with you at a specific telephone number or address.  
•	Obtain a paper copy of this notice even if you receive it electronically.  
We may ask that you make some of these requests in writing.  
  
**Tracking User Behavior**  
Regista may keep track of the websites and pages our users visit within Regista, in order to determine what Regista services are the most popular. This data is used to deliver customized content and advertising within Regista to customers whose behavior indicates that they are interested in a particular subject area.  
  
**Automatically Collected Information**  
Information about your computer hardware and software may be automatically collected by Regista. This information can include: your IP address, browser type, domain names, access times and referring website addresses. This information is used for the operation of the service, to maintain quality of the service, and to provide general statistics regarding use of the Regista website. 
  
**Children Under Thirteen**  
Regista does not knowingly collect personally identifiable information from children under the age of thirteen. If you are under the age of thirteen, you must ask your parent or guardian for permission to use the Regista website. 
  
**E-mail Communications**
From time to time, Regista may contact you via email for the purpose of providing announcements, promotional offers, alerts, confirmations, surveys, and/or other general communication. 
  
If you would like to stop receiving marketing or promotional communications via email from Regista, you may opt out of such communications by Customers may unsubscribe from emails by "replying STOP" or "clicking on the UNSUBSCRIBE button. 
  
**Changes to this Statement**  
Regista reserves the right to change this Privacy Policy from time to time. We will notify you about significant changes in the way we treat personal information by sending a notice to the primary email address specified in your account, by placing a prominent notice on our site, and/or by updating any privacy information on this page. Your continued use of the Site and/or Services available through this Site after such modifications will constitute your: (a) acknowledgment of the modified Privacy Policy; and (b) agreement to abide and be bound by that Policy. 
  
**Contact Information**  
Regista welcomes your questions or comments regarding this Statement of Privacy. If you believe that Regista has not adhered to this Statement, please contact Regista at: 
  
Regista App  
10544 Whitman Ave N  
Seattle, Washington 98133  
  
Email Address:  
info@registahealth.com  
  
Telephone number:  
2065959398  
  
Effective as of April 1, 2020  
  
